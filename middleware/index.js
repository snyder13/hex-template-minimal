'use strict';
// @flow
import type { Application, Request, Response, Next } from 'express-hex/types/express';

// parameter is type MiddlewareContext, see that definition for properties
module.exports = ({ app }: { app: Application }) => {
	app.get('/', (req: Request, res: Response, next: Next) => {
		res.type('text').send('It\'s working, sort of!');
	});
};
