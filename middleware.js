'use strict';
// @flow
import type { MiddlewareDefs } from 'express-hex/types/hex';

const mw: MiddlewareDefs = module.exports = {
	'index': {
		'description': 'Main page',
	}
};
